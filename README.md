# film_response_curves

This is an old blender add-on that was resurrected from 2.63 originally developed by Brecht Van Lommel. All credit goes to him, this is just an experiment to see if it can still be useful in current blender.

The film response curves were based on this paper: https://cave.cs.columbia.edu/old/publications/pdfs/Grossberg_CVPR03.pdf
